module.exports = {
  'LoginTest': function (client) {
    client
      .url('http://localhost:3000/index')
      .setValue('input[name="email"]', 'tyrese225@hotmail.com')
      .setValue('input[name="password]', process.env.nwpsd)
      .click('button[type="submit"]')
      .assert.containsText('h2', 'Find someone to light up with in San Francisco.')
      .end();
  }

};