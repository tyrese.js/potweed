var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var statusSchema = new Schema({

  title: String,
  host: Schema.Types.ObjectId,
  hostname: String,
  have_weed: { type: Boolean, default: false },
  real_location: String,
  filler_location: String,
  purpose: String,
  peopleInt: [ {type: Schema.Types.ObjectId, ref: 'User'} ],
  begin_time: String,
  usersAccepted: [ {type: Schema.ObjectId, ref: 'User'} ],
  peopleWanted: Number

}, { timestamps: true });

const Status = mongoose.model('Status', statusSchema);

module.exports = Status;
