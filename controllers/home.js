/**
 * GET /
 * Home page.
 */
const User = require('../models/User');
const Status = require('../models/Status');
var mongodb= require('mongodb');
var MongoClient= mongodb.MongoClient;

exports.index = (req, res) => {
	//- If no user exist send them to /index
  if (!req.user) {
    res.redirect('/index');
  } 

 Status.find(function(err, status) {
	    	res.render('home', { 
	    	title: 'Home',
	    	"status": status

	  });
	}).sort({ age: +1});
};

/**
 * GET /view_profile
 * Get user's data and view user's profile.
 */
exports.view_profile = (req, res) => {
	if(!req.user) {
		res.redirect('/index');
	 	}
	 var userid = req.params.id;
	 User.findById(req.params.id, function(err, user) {
	 	//find the user and load all of its shit
	 	if(err) return console.log(err);
	 	res.render('view_profile', { title: 'View Profile', user: user});
	 });
}